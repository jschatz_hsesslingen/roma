package edu.ltu.roma;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OverviewActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public final String TAG = "RomaLOG";
    private String customerID;
    private FirebaseAuth mAuth;

    @Override
    public void onStart() {
        super.onStart();
        Intent intent;
        // Check if user is signed in (non-null) and update UI accordingly.
        final FirebaseUser currentUser = mAuth.getInstance().getCurrentUser();
        if (currentUser.isEmailVerified()) {

        } else {
            Toast.makeText(OverviewActivity.this, "Email not verified yet. Check your Emails",
                    Toast.LENGTH_LONG).show();
            //[Start]
            //Send Email verification
            currentUser.sendEmailVerification()
                    .addOnCompleteListener(OverviewActivity.this, new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(OverviewActivity.this,
                                        "Verification email sent to " + currentUser.getEmail(),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Log.e(TAG, "sendEmailVerification", task.getException());
                                Toast.makeText(OverviewActivity.this,
                                        "Failed to send verification email.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        // [ENDE]
            FirebaseAuth.getInstance().signOut();
            intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        //updateUI(currentUser);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);


        //Get current UserEmail to connect to right database knot
        mAuth = FirebaseAuth.getInstance();
        customerID = mAuth.getCurrentUser().getUid();


        //For the hamburger we need the toolbar, where it can be placed in:
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //the topLayout in activity_main:
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_overview);

        //This is the Hamburger:
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.nav_open_drawer,
                R.string.nav_close_drawer);

        //Make the menu react to clicks
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_overview);
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setNavigationItemSelectedListener(this);



        //Connect to database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("purchases").child("customerID").child(customerID);
        final DatabaseReference myRef_total = database.getReference("totalpoints").child("customerID").child(customerID);


        //Get the last purchases to display
        myRef.orderByKey().limitToLast(3).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Delete old table entries
                TableLayout tl = findViewById(R.id.history_short);
                while (tl.getChildCount() > 1) {
                    TableRow row =  (TableRow)tl.getChildAt(1);
                    tl.removeView(row);}

                for(DataSnapshot templateSnapshot : dataSnapshot.getChildren()){
                    String date = templateSnapshot.getKey();
                    Purchase purchase = templateSnapshot.getValue(Purchase.class);
                    createTableRow(date, purchase.points, purchase.value, purchase.branch);
                }

            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


        //Get all purchases to calculate total points
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Integer total_points = 0;

                for(DataSnapshot templateSnapshot : dataSnapshot.getChildren()){

                    Purchase purchase = templateSnapshot.getValue(Purchase.class);
                    total_points += Integer.parseInt(purchase.getPoints());
                }

                //Update the TextView which shows the current points
                TextView pts =(TextView)findViewById(R.id.points);
                pts.setText(Integer.toString(total_points));

                //Update the total points in DB
                myRef_total.child("totalpoints").setValue(Integer.toString(total_points));
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        //Display the variable userID
        TextView user = (TextView)findViewById(R.id.userid);
        user.setText(mAuth.getCurrentUser().getDisplayName());


        ImageView mImageView = findViewById(R.id.qr_code);
        QRCodeWriter writer = new QRCodeWriter();
        try {
            int width = 180;
            int height = 180;
            BitMatrix bitMatrix = writer.encode(customerID, BarcodeFormat.QR_CODE, width, height);
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    bitmap.setPixel(i, j, bitMatrix.get(i, j) ? Color.BLACK: Color.WHITE);
                }
            }
            mImageView.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

    }



    //create a new Row for each of the displayed purchases
    public void createTableRow(String date, String points, String value, String branch) {
        TableLayout tl = findViewById(R.id.history_short);
        TableRow tr = new TableRow(this);
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        tr.setLayoutParams(lp);

        //Format value String to display
        String dollar = value.substring(0,value.length()-2);
        String cents = value.substring(value.length()-2);
        String value_disp = "$ " + dollar + "." + cents;

        TextView tvdate = new TextView(this);
        tvdate.setBackgroundColor(Color.WHITE);
        tvdate.setPadding(16,8,16,8);
        tvdate.setText(date);
        TextView tvpoints = new TextView(this);
        tvpoints.setBackgroundColor(Color.WHITE);
        tvpoints.setPadding(16,8,16,8);
        tvpoints.setText(points);
        TextView tvvalue = new TextView(this);
        tvvalue.setBackgroundColor(Color.WHITE);
        tvvalue.setPadding(16,8,16,8);
        tvvalue.setText(value_disp);
        TextView tvbranch = new TextView(this);
        tvbranch.setBackgroundColor(Color.WHITE);
        tvbranch.setPadding(16,8,16,8);
        tvbranch.setText(branch);

        //Add TextViews to TableRow
        tr.addView(tvdate);
        tr.addView(tvpoints);
        tr.addView(tvvalue);
        tr.addView(tvbranch);

        //Add TableRow to TableLayout
        tl.addView(tr);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent = null;

        switch(id) {
            case R.id.nav_rank:
                intent = new Intent(this, RankingActivity.class);
                break;
            case R.id.nav_history:
                intent = new Intent(this, HistoryActivity.class);
                break;
            case R.id.nav_logout:
                FirebaseAuth.getInstance().signOut();
                intent = new Intent(this, LoginActivity.class);
                break;
            default:
                intent = new Intent(this, OverviewActivity.class);
        }

            startActivity(intent);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_overview);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    //Close the menu when you click the back button
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_overview);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


}
