package edu.ltu.roma;

/**
 * Created by Julian on 12.10.2017.
 */

public class Purchase {

    public String type;
    public String value;
    public String points;
    public String branch;



    public Purchase(){
        //default constructor for "dataSnapshot.getValue(Purchase.class);" in OverviewActivity
    }
        public Purchase(String value, String points, String branch, String type) {
        this.value = value;
        this.points = points;
            this.branch = branch;
            this.type = type;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}
