package edu.ltu.roma;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class HistoryActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    public final String TAG = "roma";
    private String customerID;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        //Get current UserEmail to connect to right database knot
        mAuth = FirebaseAuth.getInstance();
        customerID = mAuth.getCurrentUser().getUid();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //the topLayout in activity_main:
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_history);

        //This is the Hamburger:
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.nav_open_drawer,
                R.string.nav_close_drawer);

        //Make the menu react to clicks
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_history);
        navigationView.getMenu().getItem(2).setChecked(true);
        navigationView.setNavigationItemSelectedListener(this);



        //Connect to database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("purchases").child("customerID").child(customerID);


        //Get all purchases to display history
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                update_history(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        //Spinner to filter the displayed purchases by branches
        Spinner spinner = (Spinner)findViewById(R.id.spinner_branch);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                myRef.addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        update_history(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        Log.w(TAG, "Failed to read value.", error.toException());
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }



    public void update_history(DataSnapshot dataSnapshot){
        //Delete old table entries
        TableLayout tl = findViewById(R.id.tableLayout);
        while (tl.getChildCount() > 1) {
            TableRow row =  (TableRow)tl.getChildAt(1);
            tl.removeView(row);}


        for(DataSnapshot templateSnapshot : dataSnapshot.getChildren()){
            String date = templateSnapshot.getKey();
            Purchase purchase = templateSnapshot.getValue(Purchase.class);

            Spinner spbranch = (Spinner)findViewById(R.id.spinner_branch);
            String stbranch = spbranch.getSelectedItem().toString();

            if(stbranch.equals("All branches")){
                createTableRow(date, purchase.points, purchase.value, purchase.branch);
            }else if(stbranch.equals(purchase.branch)) {
                createTableRow(date, purchase.points, purchase.value, purchase.branch);
            }
        }
    }

    //create a new Row for each of the displayed purchases
    public void createTableRow(String date, String points, String value, String branch) {
        TableLayout tl = findViewById(R.id.tableLayout);
        TableRow tr = new TableRow(this);
        TableLayout.LayoutParams lp = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
        tr.setLayoutParams(lp);

        //Format value String to display
        String dollar = value.substring(0,value.length()-2);
        String cents = value.substring(value.length()-2);
        String value_disp = "$ " + dollar + "." + cents;

        TextView tvLeft = new TextView(this);
        tvLeft.setBackgroundColor(Color.WHITE);
        tvLeft.setPadding(16,8,16,8);
        tvLeft.setText(date);
        TextView tvCenter = new TextView(this);
        tvCenter.setBackgroundColor(Color.WHITE);
        tvCenter.setPadding(16,8,16,8);
        tvCenter.setText(points);
        TextView tvRight = new TextView(this);
        tvRight.setBackgroundColor(Color.WHITE);
        tvRight.setPadding(16,8,16,8);
        tvRight.setText(value_disp);
        TextView tvbranch = new TextView(this);
        tvbranch.setBackgroundColor(Color.WHITE);
        tvbranch.setPadding(16,8,16,8);
        tvbranch.setText(branch);


        tr.addView(tvLeft);
        tr.addView(tvCenter);
        tr.addView(tvRight);
        tr.addView(tvbranch);

        tl.addView(tr);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent = null;

        switch(id) {
            case R.id.nav_rank:
                intent = new Intent(this, RankingActivity.class);
                break;
            case R.id.nav_history:
                intent = new Intent(this, HistoryActivity.class);
                break;
            case R.id.nav_logout:
                FirebaseAuth.getInstance().signOut();
                intent = new Intent(this, LoginActivity.class);
                break;
            default:
                intent = new Intent(this, OverviewActivity.class);
        }

        startActivity(intent);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_history);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    //Close the menu when you click the back button
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_history);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


}
