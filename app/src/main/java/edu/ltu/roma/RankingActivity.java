package edu.ltu.roma;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RankingActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String customerID;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        //Get current UserEmail to connect to right database knot
        mAuth = FirebaseAuth.getInstance();
        customerID = mAuth.getCurrentUser().getUid();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //the topLayout in activity_main:
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_rank);

        //This is the Hamburger:
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.nav_open_drawer,
                R.string.nav_close_drawer);

        //Make the menu react to clicks
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_rank);
        navigationView.getMenu().getItem(1).setChecked(true);
        navigationView.setNavigationItemSelectedListener(this);

        //Connect to database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("ranking");


        //Get ranking from Firebase
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Integer first = 0;
                Integer second = 0;
                Integer third = 0;

                //Delete old table entries
                TableLayout tl = findViewById(R.id.ranking);
                while (tl.getChildCount() > 1) {
                    TableRow row =  (TableRow)tl.getChildAt(1);
                    tl.removeView(row);}


                for(DataSnapshot templateSnapshot : dataSnapshot.getChildren()){
                    Integer pos = Integer.parseInt(templateSnapshot.getKey());

                    switch(pos){
                        case 1:
                            first = Integer.parseInt(templateSnapshot.child("points").getValue().toString());
                            createTableRow(pos, first);
                            break;
                        case 2:
                            second = Integer.parseInt(templateSnapshot.child("points").getValue().toString());
                            createTableRow(pos, second);
                            break;
                        case 3:
                            third = Integer.parseInt(templateSnapshot.child("points").getValue().toString());
                            createTableRow(pos, third);
                            break;
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w("TAG", "Failed to read value.", error.toException());
            }
        });



    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent = null;

        switch(id) {
            case R.id.nav_rank:
                intent = new Intent(this, RankingActivity.class);
                break;
            case R.id.nav_history:
                intent = new Intent(this, HistoryActivity.class);
                break;
            case R.id.nav_logout:
                FirebaseAuth.getInstance().signOut();
                intent = new Intent(this, LoginActivity.class);
                break;
            default:
                intent = new Intent(this, OverviewActivity.class);
        }

        startActivity(intent);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_rank);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    //Close the menu when you click the back button
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_rank);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //create a new Row for each of the displayed purchases
    public void createTableRow(Integer pos, Integer points) {
        TableLayout tl = findViewById(R.id.ranking);
        TableRow tr = new TableRow(this);
        TableLayout.LayoutParams lp = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
        tr.setLayoutParams(lp);

        TextView position = new TextView(this);
        position.setBackgroundColor(Color.WHITE);
        position.setTextSize(24);
        position.setPadding(16,8,16,8);
        position.setText(pos.toString());
        TextView tvpoints = new TextView(this);
        tvpoints.setBackgroundColor(Color.WHITE);
        tvpoints.setTextSize(24);
        tvpoints.setPadding(16,8,16,8);
        tvpoints.setText(points.toString());

        //Add TextViews to TableRow
        tr.addView(position);
        tr.addView(tvpoints);

        //Add TableRow to TableLayout
        tl.addView(tr);
    }


}
